import React from "react";
import  ReactDOM  from 'react-dom';

function Products(){
    return(
        <div>
            <h3>
                Кількість днів у місяцях року.
            </h3>
            <table>
                <thead>
                    <tr>
                        <td>12 місяців</td>
                        <th>Січень</th>
                        <th>Лютий</th>
                        <th>Березень</th>
                        <th>Квітень</th>
                        <th>Травень</th>
                        <th>Червень</th>
                        <th>Липень</th>
                        <th>Серпень</th>
                        <th>Вересень</th>
                        <th>Жовтень</th>
                        <th>Листопад</th>
                        <th>Грудень</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Кількість днів</td>
                        <td>31</td>
                        <td>28/29</td>
                        <td>31</td>
                        <td>30</td>
                        <td>31</td>
                        <td>30</td>
                        <td>31</td>
                        <td>31</td>
                        <td>30</td>
                        <td>31</td>
                        <td>30</td>
                        <td>31</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

ReactDOM.render(<Products></Products>, document.getElementById('one'))